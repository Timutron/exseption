/*
 Given 3 int values, a b c, return their sum. However, if one of the values is the same as another of the values,
 it does not count towards the sum.
*/
public class Logic2LoneSum {
    public int loneSum(int a, int b, int c) {
        if (a == b && a == c)
            return 0;
        else if (a != b && a != c && b != c)
            return a + b + c;
        else
            return a == b ? c : (a == c ? b : (c == b ? a : 0));
    }
}
