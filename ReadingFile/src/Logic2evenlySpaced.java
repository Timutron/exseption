/*
  Given three ints, a b c, one of them is small, one is medium and one is large.
  Return true if the three values are evenly spaced, so the difference
  between small and medium is the same as the difference between medium and large.
*/

public class Logic2evenlySpaced {
    public boolean evenlySpaced(int a, int b, int c) {
        double x = (double)(a + b + c)/3;
        if (x == a || x == b || x == c)
            return true;
        else
            return false;
    }
}
